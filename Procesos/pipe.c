#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#define MAX 1024

void main()
{
    char command[MAX];
    char buffer[MAX];
    char *param = (char *)malloc(20);
    int fds[2];
    pid_t childpid;
    char *argv[3];

    printf("Elija una opcion:\n");
    printf("Ingresar comando(exit para terminar el programa):\n");
    scanf("%s", buffer);
    strcpy(command, buffer);

    while (strcmp(command, "exit") != 0)
    {
        printf("Ingresar parametro(si no quiere parametro escriba no):\n");
        scanf("%s", param);
        if (strcmp(param, "no") == 0)
        {
            argv[1] = NULL;
        }else {
            argv[1] = param;
        }

        pipe(fds);

        if ((childpid = fork()) == -1)
        {
            perror("fork");
            exit(1);
        }

        if (childpid == 0)
        {
            /* Child process closes up input side of pipe */
            close(fds[0]);

            argv[0] = "";
            argv[2] = NULL;
            execvp(command, argv);
            write(fds[1], stdout, MAX);
            exit(0);
        }
        else
        {
            /* Parent process closes up output side of pipe */
            close(fds[1]);
            printf("Salida:\n");
            read(fds[0], stdout, MAX);
            printf("\nElija una opcion:\n");
            printf("Ingresar comando(exit para terminar el programa):\n");
            scanf("%s", buffer);
            strcpy(command, buffer);

        }
    }

    exit(0);
}
