#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

static clock_t *total;

void main()
{
    int tareas;
    int i,j;
    pid_t fpid;
    clock_t start_t, end_t, total_t;
    
    printf("Ingrese el numero de tareas:\n");
    scanf("%d", &tareas);

    total = mmap(NULL, sizeof *total, PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    

    for (i = 0; i < tareas; i++)
    {
        
        fpid = fork();
        if (fpid == -1)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        if (fpid == 0)
        {
            
            start_t = clock();
            for(j=0;j<10000000;j++) {

            }
            end_t = clock();
            total_t = (end_t - start_t) / (double) CLOCKS_PER_SEC;
            total = total + total_t;
            exit(0);
        }
        else
        {
            
            printf("Tiempo promedio es: %ld\n", total);
            munmap(total, sizeof *total);
        }
    }

    wait(NULL);

    exit(0);
}