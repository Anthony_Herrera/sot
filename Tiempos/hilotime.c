#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <pthread.h>

clock_t start_t, end_t, total_t, total;

void *threadFunction(void *args)
{
    start_t = clock();
    for (int j = 0; j < 10000000; j++)
    {
    }
    end_t = clock();
    total_t = (end_t - start_t) / (double) CLOCKS_PER_SEC;
    total = total + total_t;
    printf("Tiempo promedio es: %ld\n", total);
}

void main()
{
    int tareas;
    int i;
    pid_t tid;
    pthread_t mythread;

    printf("Ingrese el numero de tareas:\n");
    scanf("%d", &tareas);

    for (i = 0; i < tareas; i++)
    {
        pthread_create(&mythread, NULL, threadFunction, (void *)i);  
    }
    printf("Tiempo promedio es: %ld segundos", total);
    pthread_exit(NULL);

    exit(0);
}